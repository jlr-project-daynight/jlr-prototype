using System;
using UnityEngine;

public class JLRUtils
{
    public static DateTime EpochToDT(double unixTimeStamp)
    {
        // Unix timestamp is seconds past epoch
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dtDateTime;
    }
    public static float RangeConv(float input, float x1, float x2, float y1, float y2)
    {
        return
        (
            (
                (input - x1)
                * (y2 - y1)
            )
            / (x2 - x1)
        )
        + y1;
    }
}
