﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    public GameObject sun;

    private DateTime sunrise, sunset, currentLocalTime, timeStartedRunnning;


    // Start is called before the first frame update
    void Start()
    {
        GetAstroData();
        PositionSun(sunrise, sunset, currentLocalTime);
        sun.transform.SetPositionAndRotation(sun.transform.position, new Quaternion(0, 0, 0, 0));
        timeStartedRunnning = DateTime.Now;
    }

    private void GetAstroData()
    {
        //Leaving these here for testing.
        //currentLocalTime = new DateTime(2019, 06, 28, 23, 38, 00);
        //sunrise = new DateTime(2019, 06, 28, 04, 47, 00);
        //sunset = new DateTime(2019, 06, 28, 21, 35, 00);

        sunrise = ConfigManager.APIObject.sunrise;
        sunset = ConfigManager.APIObject.sunset;
        currentLocalTime = ConfigManager.APIObject.localTime;


    }

    public void PositionSun(DateTime sunrise, DateTime sunset, DateTime currentLocalTime)
    {
        float percentOfTimeframe = 0;

        if (currentLocalTime >= sunrise && currentLocalTime <= sunset)
        {
            percentOfTimeframe = GetPercentOfTimeframe(sunrise, sunset, currentLocalTime);
            sun.transform.Rotate((-percentOfTimeframe * 180) + 180, 0, 0);
            sun.GetComponent<Light>().intensity = 1;
        }

        else if (currentLocalTime < sunrise)
        {
            percentOfTimeframe = GetPercentOfTimeframe(sunset, sunrise.AddDays(1), currentLocalTime.AddDays(1));
            sun.transform.Rotate((-percentOfTimeframe * 180), 0, 0);
            sun.GetComponent<Light>().intensity = (float)Math.Pow(Math.Asin(percentOfTimeframe - 0.159), 4);
        }

        else if (currentLocalTime > sunset)
        {
            percentOfTimeframe = GetPercentOfTimeframe(sunset, sunrise.AddDays(1), currentLocalTime);
            sun.transform.Rotate((-percentOfTimeframe * 180), 0, 0);
            sun.GetComponent<Light>().intensity = 1 - percentOfTimeframe;
            sun.GetComponent<Light>().intensity = 1 - (float)Math.Pow(Math.Asin(percentOfTimeframe - 0.159), 4);
        }

        if (sun.GetComponent<Light>().intensity < 0.1)
            sun.GetComponent<Light>().intensity = 0;
    }
            

    /*
     * Returns percentage as a float between 0 and 1 
     */
    private float GetPercentOfTimeframe(DateTime startTime, DateTime endTime, DateTime currentTime)
    {
        TimeSpan timeframe = endTime - startTime;
        TimeSpan timeSoFar = currentTime - startTime;

        float percentage = (float)timeSoFar.TotalMinutes / (float)timeframe.TotalMinutes;
        if (percentage < 0)
            percentage *= -1;
        Debug.Log("percent: " + percentage);
        return percentage;
    }
}
