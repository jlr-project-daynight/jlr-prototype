﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{

    public float rotationspeed;
    public GameObject carObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        

        carObject.transform.Rotate(new Vector3 (0 , rotationspeed * Time.deltaTime, 0));

    }
}
