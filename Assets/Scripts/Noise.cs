﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noise : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Texture2D image = new Texture2D(2048, 2048);
        float perlin;

        for(int x = 0; x < image.width; x++)
        {
            for(int y = 0; y < image.height; y++)
            {
                perlin = Mathf.PerlinNoise((float)x / 256f, (float)y / 256f);
                image.SetPixel(x, y, new Color(perlin, perlin, perlin));
            }
        }

        byte[] bytes = image.EncodeToPNG();
        System.IO.File.WriteAllBytes(".\\Assets\\PerlinNoiseClouds.png", bytes);
    }

    // Update is called once per frame
    void Update()
    {
        Mathf.PerlinNoise(0.1f, 0.9f);
    }
}
