﻿using System.IO;
using UnityEngine;
using UnityEditor;
using System;

public class ConfigManager : EditorWindow
{
    public static Config config = new Config();
    public enum API { OpenWeatherMap, Apixu };
    public static API api;
    public static string APIKey;
    bool ToggleG1;
    public static APIObject APIObject = new ApixuAPI();
    public static APIObject API1 = new OpenWeatherMapAPI();
    
    private void OnGUI()
    {
        GUILayout.Label("API Settings", EditorStyles.boldLabel);
        api = (API)EditorGUILayout.EnumPopup("API: ", api);
        APIKey = EditorGUILayout.TextField("API Key: ",APIKey);
        
        GUILayout.BeginHorizontal("box");
        GUILayout.Space(40);
        if (GUILayout.Button("Save"))
        {
            Save();
        }
        GUILayout.Space(40);
        if (GUILayout.Button("Load")){
            config = Load();
        }
        GUILayout.Space(40);
        GUILayout.EndHorizontal();
        GUILayout.Space(20);

        ToggleG1 = EditorGUILayout.BeginToggleGroup("Tick To disable Safety", ToggleG1);
          if (GUILayout.Button("Generate Defaults")){
            GenerateDefaultCfg();
        }
        EditorGUILayout.EndToggleGroup();
    }
    [MenuItem("Config/Windows/Config Window")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(ConfigManager));
    }

    [MenuItem("Config/Save")]
    public static void Save()
    {
        config.API = api;
        config.APIKey = APIKey;
        string path = Application.dataPath;
        if (Application.platform == RuntimePlatform.OSXPlayer)
        {
            path += "/../../APIConfig.cfg";
        }
        else if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            path += "/../APIConfig.cfg";
        }
        else
        {
            path += "/APIConfig.cfg";
        }
        APIObject.GetData(APIKey,"London");
        Debug.Log(APIObject.GetAPIName() + ": " + APIObject.GetLatitude() + "," + APIObject.GetLongitude());
        string json = JsonUtility.ToJson(config);
        using (StreamWriter outputFile = new StreamWriter(path))
        {
            outputFile.WriteLine(json);
        }
    }
    public static Config Load()
    {

        string path = Application.dataPath;
        if (Application.platform == RuntimePlatform.OSXPlayer)
        {
            path += "/../../APIConfig.cfg";
        }
        else if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            path += "/../APIConfig.cfg";
        }
        else
        {
            path += "/APIConfig.cfg";
        }
        if (File.Exists(path))
        {
            using (StreamReader configFile = new StreamReader(path))
            {
                string json = configFile.ReadLine();
                Config conf = JsonUtility.FromJson<Config>(json);
                api = conf.API;
                APIKey = conf.APIKey;
                return conf;
            }
        }
        else
        {
            GenerateDefaultCfg();
            return Load();
        }
    }

    [MenuItem("Config/Generate Fresh Config")]
    public static void GenerateDefaultCfg()
    {
        string path = Application.dataPath;
        if (Application.platform == RuntimePlatform.OSXPlayer)
        {
            path += "/../../APIConfig.cfg";
        }
        else if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            path += "/../APIConfig.cfg";
        }
        else
        {
            path += "/APIConfig.cfg";
        }

        string json = JsonUtility.ToJson(config);
        using (StreamWriter outputFile = new StreamWriter(path))
        {
            outputFile.WriteLine(json);
        }
    }
}

[Serializable]
public class Config
{
    public ConfigManager.API API;
    public string APIKey;
}