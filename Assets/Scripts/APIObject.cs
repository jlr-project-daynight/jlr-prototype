﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class APIObject
{
    public string APIName { get => GetAPIName(); }
    public float latitude { get => GetLatitude(); }
    public float longitude { get => GetLongitude(); }
    public float humidity { get => GetHumidity(); }
    public float windSpeed { get => GetWindSpeed(); }
    public float windDegrees { get => GetWindDegrees(); }
    public float temp { get => GetTemperature(); }
    public float pressure { get => GetPressure(); }
    public float tempMin { get => GetTemperatureMinimum(); }
    public float tempMax { get => GetTemperatureMaximum(); }
    public float cloudsPercent { get => GetCloudiness(); }
    public DateTime sunrise { get => GetSunrise(); }
    public DateTime sunset { get => GetSunset(); }
    public DateTime localTime { get => GetLocalTime(); }
    public float timezone { get => GetTimezone(); }
    public string json;
    public ApixuAPIData ApixuAPIData;
    public OWMAPIData OWMAPIData;
    public virtual void GetData(string APIKey, string query)
    {
        return;
    }

    public virtual string GetAPIName()
    {
        return "null";
    }
    public virtual float GetLatitude()
    {
        return 0.0f;
    }
    public virtual float GetLongitude()
    {
        return 0.0f;
    }
    public virtual float GetHumidity()
    {
        return 0.0f;
    }
    public virtual float GetWindSpeed()
    {
        return 0.0f;
    }
    public virtual float GetWindDegrees()
    {
        return 0.0f;
    }
    public virtual float GetTemperature()
    {
        return 0.0f;
    }
    public virtual float GetPressure()
    {
        return 0.0f;
    }
    public virtual float GetTemperatureMinimum()
    {
        return 0.0f;
    }
    public virtual float GetTemperatureMaximum()
    {
        return 0.0f;
    }
    public virtual float GetCloudiness()
    {
        return 0.0f;
    }
    public virtual DateTime GetSunrise()
    {
        return new DateTime();
    }
    public virtual DateTime GetSunset()
    {
        return new DateTime();
    }
    public virtual DateTime GetLocalTime()
    {
        return new DateTime();
    }
    public virtual float GetTimezone()
    {
        return 0.0f;
    }
}
