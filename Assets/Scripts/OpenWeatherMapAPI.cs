﻿using System;
using System.Net.Http;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RestSharp;

[Serializable]
public class OpenWeatherMapAPI : APIObject
{
    public new string json;
    public new OWMAPIData OWMAPIData;
    RestClient client = new RestClient("https://api.openweathermap.org");

    public new float longitude { get => longitude; }
    public override string GetAPIName()
    {
        return "OpenWeatherMap";
    }
    public override float GetLatitude()
    {
        if (OWMAPIData != null) { return OWMAPIData.coord.lat; }
        return 0.0f;
    }
    public override float GetLongitude()
    {
        if (OWMAPIData != null) { return OWMAPIData.coord.lon; }
        return 0.0f;
    }
    public override float GetHumidity()
    {
        if (OWMAPIData != null) { return OWMAPIData.main.humidity; }
        return 0.0f;
    }
    public override float GetWindSpeed()
    {
        if (OWMAPIData != null) { return OWMAPIData.wind.speed; }
        return 0.0f;
    }
    public override float GetWindDegrees()
    {
        if (OWMAPIData != null) { return OWMAPIData.wind.deg; }
        return 0.0f;
    }
    public override float GetTemperature()
    {
        if (OWMAPIData != null) { return OWMAPIData.main.temp; }
        return 0.0f;
    }
    public override float GetPressure()
    {
        if (OWMAPIData != null) { return OWMAPIData.main.pressure; }
        return 0.0f;
    }
    public override float GetTemperatureMinimum()
    {
        if (OWMAPIData != null) { return OWMAPIData.main.temp_min; }
        return 0.0f;
    }
    public override float GetTemperatureMaximum()
    {
        if (OWMAPIData != null) { return OWMAPIData.main.temp_max; }
        return 0.0f;
    }
    #pragma warning disable 0436
    public override float GetCloudiness()
    {
        if (OWMAPIData != null) { return JLRUtils.RangeConv(OWMAPIData.clouds.all,1,100,0,1); }
        return 0.0f;
    }
    public override DateTime GetSunrise()
    {
        if (OWMAPIData != null) { return JLRUtils.EpochToDT(OWMAPIData.sys.sunrise); }
        return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
    }
    public override DateTime GetSunset()
    {
        if (OWMAPIData != null) { return JLRUtils.EpochToDT(OWMAPIData.sys.sunset); }
        return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
    }
    #pragma warning restore 0436
    public override float GetTimezone()
    {
        if (OWMAPIData != null) { return OWMAPIData.timezone; }
        return 0.0f;
    }

    public override void GetData(string APIKey, string city)
    {
        RestRequest request = new RestRequest("data/2.5/weather");
        request.AddParameter("q", city);
        request.AddParameter("appid", APIKey);
        IRestResponse response = client.Execute(request);
        json = response.Content;
        OWMAPIData = JsonUtility.FromJson<OWMAPIData>(json);
        Debug.Log(json);
    }
}

[Serializable]
public class OWMAPIData
{
    public coord coord;
    public weather weather;
    public main main;
    public wind wind;
    public clouds clouds;
    public int dt;
    public sys sys;
    public int timezone;
    public int id;
    public string name;
    public int cod;
}

[Serializable]
public class coord
{
    public float lon;
    public float lat;
}
[Serializable]
public class weather
{
    public int id;
    public string main;
    public string description;
    public string icon;
}
[Serializable]
public class main
{
    public float temp;
    public float pressure;
    public float humidity;
    public float temp_min;
    public float temp_max;
}
[Serializable]
public class wind
{
    public float speed;
    public float deg;
}
[Serializable]
public class clouds
{
    public float all;
}
[Serializable]
public class sys
{
    public string country;
    public double sunrise;
    public double sunset;
}
