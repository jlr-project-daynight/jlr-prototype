using System;
using System.Net.Http;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RestSharp;

[Serializable]
public class ApixuAPI : APIObject
{
    public new string json;
    public new ApixuAPIData ApixuAPIData;
    RestClient client = new RestClient("http://api.apixu.com");

    public new float longitude { get => longitude; }
    public override string GetAPIName()
    {
        return "Apixu";
    }
    public override float GetLatitude()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.location.lat; }
        return 0.0f;
    }
    public override float GetLongitude()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.location.lon; }
        return 0.0f;
    }
    public override float GetHumidity()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.current.humidity; }
        return 0.0f;
    }
    public override float GetWindSpeed()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.current.wind_kph / 3.6f; }
        return 0.0f;
    }
    public override float GetWindDegrees()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.current.wind_degree; }
        return 0.0f;
    }
    public override float GetTemperature()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.current.temp_c; }
        return 0.0f;
    }
    public override float GetPressure()
    {
        if (ApixuAPIData != null) { return ApixuAPIData.current.pressure_mb * 100; }
        return 0.0f;
    }
    #pragma warning disable 0436
    public override float GetCloudiness()
    {
        if (ApixuAPIData != null) { return JLRUtils.RangeConv(ApixuAPIData.current.cloud,1,100,0,1); }
        return 0.0f;
    }
    public override DateTime GetSunrise()
    {
        //if (ApixuAPIData != null) { return JLRUtils.EpochToDT(ApixuAPIData.sys.sunrise); }
        return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
    }
    public override DateTime GetSunset()
    {
        //if (ApixuAPIData != null) { return JLRUtils.EpochToDT(ApixuAPIData.sys.sunset); }
        return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
    }
    #pragma warning restore 0436

    public override void GetData(string APIKey, string city)
    {
        RestRequest request = new RestRequest("v1/current.json");
        request.AddParameter("key", APIKey);
        request.AddParameter("q", city);
        IRestResponse response = client.Execute(request);
        json = response.Content;
        ApixuAPIData = JsonUtility.FromJson<ApixuAPIData>(json);
    }
}
[Serializable]
public class ApixuAPIData
{
    public location location;
    public current current;
}

[Serializable]
public class location
{
    public string name;
    public string region;
    public string country;
    public float lat;
    public float lon;
    public string tz_id;
    public double localtime_epoch;
    public string localtime;
}

[Serializable]
public class current
{
    public double last_updated_epoch;
    public string last_updated;
    public float temp_c;
    public float temp_f;
    public int is_day;
    public condition condition;
    public float wind_mph;
    public float wind_kph;
    public int wind_degree;
    public string wind_dir;
    public float pressure_mb;
    public float pressure_in;
    public float precip_in;
    public float precip_mm;
    public int humidity;
    public int cloud;
    public float feelslike_c;
    public float feelslike_f;
    public float vis_km;
    public float vis_miles;
    public float uv;
    public float gust_mph;
    public float gust_kph;
}

[Serializable]
public class condition
{
    public string text;
    public string icon;
    public int code;
}