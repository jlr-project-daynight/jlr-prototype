﻿Shader "JLR/ObjectShaders/Snow"
{
	Properties
	{
		[Header(MainTextures)]
		_Color("Tint", Color) = (0, 0, 0, 1)
		[NoScaleOffset]
		_MainTex("Texture", 2D) = "white" {}

		[Normal]	[NoScaleOffset]
		_NormalMap("Normal", 2D) = "bump" {}

		[NoScaleOffset]
		_SpecularMap("Specular", 2D) = "white" {}
		//_Smoothness("Smoothness", Range(0, 1)) = 0
		//_SnowAmount("Snow Amount", Range (0, 1)) = 0.5
		[HDR] _Emission("Emission", color) = (0,0,0)

		//_TextureScale("Texture Scale", float) = 
		[Header()][Header(Dissolve Settings)]
		_DissolveNoiseSize("Dissolve Noise Size", Range(0.01, 1)) = 0.5
		_DissolveAmount("% Dissolved Amount", Range(0, 1)) = 0.5

		[Header()][Header(Dissolve Edge Settigns)]
		[HDR]_EdgeColor("Color", Color) = (1, 1, 1, 1)
		_EdgeRange("Range", Range(0, .3)) = 0.1
		_EdgeFalloff("Falloff", Range(0.001, .3)) = 0.1
	}

	SubShader
		{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+1"}
		//Ztest checks zbuffer for if fragments are in the same place. 
		//checking the depth of the fragment that is currently rendered
		//if the fragment is equal to a current fragment overwrite it
		ZTest LEqual

		CGPROGRAM

		#pragma surface surf StandardSpecular fullforwardshadows vertex:vert
		//#pragma surface surf Lambert Alpha
		#pragma target 3.0

		#include "WhiteNoise.cginc"		//Import methods for white noise generation

		//textures - Main Texture, Normal Map, Specular Map			
		sampler2D _MainTex;				
		sampler2D _NormalMap;
		sampler2D _SpecularMap;
		float2 uv_NormalMap;	
		fixed4 _Color;
		//half _Smoothness;
		//half _SnowAmount;
		half3 _Emission;
		half _TextureScale;
	
		//Dissolve settings
		float _DissolveAmount;
		float _DissolveNoiseSize;
		float _Jitter;
	
		//Edge settings
		float3 _EdgeColor;
		float _EdgeRange;
		float _EdgeFalloff;
	

		//accessed by the fragment shader
		struct Input{
			float2 uv_MainTex;
			float2 uv_NormalMap;
			float2 uv_SpecularMap;
			float2 uv_DissolveTex;
			float4 pos;
			float3 vertexNormal;//hold vertex nor
		};
//

		//perlin noise methods
		float easeIn(float interpolator){
			return interpolator * interpolator;
		}

		float easeOut(float interpolator){
			return 1 - easeIn(1 - interpolator);
		}

		float easeInOut(float interpolator){
			float easeInValue = easeIn(interpolator);
			float easeOutValue = easeOut(interpolator);
			return lerp(easeInValue, easeOutValue, interpolator);
		}

		float perlinNoise(float3 value){
			float3 fraction = frac(value);

			float interpolatorX = easeInOut(fraction.x);
			float interpolatorY = easeInOut(fraction.y);
			float interpolatorZ = easeInOut(fraction.z);

			float3 cellNoiseZ[2];
			[unroll]
			for(int z=0;z<=1;z++)
			{
				float3 cellNoiseY[2];
				[unroll]
				for(int y=0;y<=1;y++)
				{
					float3 cellNoiseX[2];
					[unroll]
					for(int x=0;x<=1;x++)
					{
						float3 cell = floor(value) + float3(x, y, z);
						float3 cellDirection = rand3dTo3d(cell) * 2 - 1;
						float3 compareVector = fraction - float3(x, y, z);
						cellNoiseX[x] = dot(cellDirection, compareVector);
					}
					cellNoiseY[y] = lerp(cellNoiseX[0], cellNoiseX[1], interpolatorX);
				}
				cellNoiseZ[z] = lerp(cellNoiseY[0], cellNoiseY[1], interpolatorY);
			}
			float3 noise = lerp(cellNoiseZ[0], cellNoiseZ[1], interpolatorZ);
			return noise;
		}		

		//transfer the vertex normal to the input structure
		void vert (inout appdata_tan v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.vertexNormal = mul(unity_ObjectToWorld, v.normal);
			o.pos = v.vertex * 0.5;
		}
	
		void surf(Input i, inout SurfaceOutputStandardSpecular o){
			
			//for the dissolving pattern
			float3 HighFreqValue = i.pos / _DissolveNoiseSize;			//Low frequency noise generation for large patterns
			float3 LowFreqValue = i.pos / _DissolveNoiseSize / 10;		//High frequency noise genereation for small detailed patterns

			//get noise and adjust it to be 0-1 range
			float PrimaryNoise = perlinNoise(HighFreqValue) + 0.5;		
			float SecondaryNoise = perlinNoise(LowFreqValue);
			
			float finalNoise = PrimaryNoise + SecondaryNoise;			//Combine the 2 levels of noise into 1 value.
			//apply the noise to the dissolve settings
			float dissolve = finalNoise;								//Set the dissolve value to = the noise value

			dissolve = dissolve * 0.999;
			float isVisible = dissolve - _DissolveAmount * 2.1 + 0.6;		
			clip(isVisible);											//Set visibility of the dissolved amount
	

			//edge for the dissolve texture
			float isedge = smoothstep(_EdgeRange + _EdgeFalloff, _EdgeRange, isVisible);
			float3 edge = isedge * _EdgeColor;											
	

			//main texture settings
			fixed4 col = tex2D(_MainTex, i.pos.xz);
			col *= _Color;
			o.Normal = UnpackNormal(tex2D(_NormalMap, i.pos.xz));
						
			//discard the normal map that isnt facing upwards
			if (i.vertexNormal.y < 0 + ((i.pos.x * i.pos.z) % 1.0) / 2.0)
			{
				discard;
			}

			//Combine the final settings together
			o.Specular = tex2D(_SpecularMap, i.pos.xz);
			o.Albedo += col;
			//o.Metallic = _Metallic;
			o.Smoothness = 0.2;
			o.Emission = _Emission + edge;
		}
		
		ENDCG
	}
	FallBack "Standard"

	//thanks kieran
}
