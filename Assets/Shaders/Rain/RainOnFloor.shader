﻿Shader "Custom/RainOnFloor"
{
	//Properties, the main attributes to what the shader is comprised of.
	//Non hidden elements can be accessed and modified within the unity editor through the inspector.

	Properties
	{
		[Header()][Header (Main Texture)]
		_MainTex("Base Texture" , 2D) = "White" {}
		_Color("Texture Colour", Color) = (0, 0, 0, 1)

		[Header()][Header (Weather Texture)]
		_WeatherMaterial ("Weather Material", 2D) = "White" {}

		[Header()][Header(Heightmap Dissolve)]
		_Heightmap("Heightmap", 2D) = "White" {}
	}

	SubShader
	{
		Tags
		{"RenderType" = "Opaque""Queue" = "Geometry"}

		Pass
		{
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;

			sampler2D _WeatherMaterial;
			float4 _WeatherMaterial_ST;

			sampler2D _Heightmap;
		

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			
			/*
			struct appdatatwo {
				float4 vertex : POSITION;
			};
			*/

			struct v2f {
				float4 MainTexPosition : SV_POSITION;
				//float4 WeathTexPosition : SV_WPOSITION;
				float2 uv : TEXCOORD0;

			};

			//vert shader
			//TextureUVMap
			
			v2f vert(appdata t) {

				//MainTex
				v2f o;
				o.MainTexPosition = UnityObjectToClipPos(t.vertex);
				o.uv = TRANSFORM_TEX(t.uv, _MainTex);


				//PlanarTex
				v2f p;
				p.MainTexPosition = UnityObjectToClipPos(t.vertex);
				float4 worldPos = mul(unity_ObjectToWorld, t.vertex);
				p.uv = TRANSFORM_TEX(worldPos.xz, _WeatherMaterial);
				return o;
			
			}
			
			/* 
			//PlanarMap			
			v2f vert2(appdata p){

				v2f o;
				o.MainTexPosition = UnityObjectToClipPos(p.vertex);
				float4 worldPos = mul(unity_ObjectToWorld, p.vertex);
				o.uv = TRANSFORM_TEX(worldPos.xz, _WeatherMaterial);
				return o;
			}
			*/
						
			//fragment shader
			fixed4 frag(v2f i) : SV_TARGET{
				fixed4 col = tex2D(_MainTex, i.uv);
				//col  += tex2D(_WeatherMaterial, i.uv);		
				col *= _Color;
				return col;
			}

			ENDCG
		}
	}

    //FallBack "Diffuse"
}
