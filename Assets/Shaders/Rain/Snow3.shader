﻿Shader "Unit/Snow3"
{
	Properties
	{
		[Header(MainTextures)]
		_Color("Tint", Color) = (0, 0, 0, 1)
		_MainTex("Texture", 2D) = "white" {}
		_NormalMap("Normal", 2D) = "bump" {}
		_SpecularMap("Specular", 2D) = "white" {}
		_Smoothness("Smoothness", Range(0, 1)) = 0
		_SnowAmount("Snow Amount", Range (0, 1)) = 0.5
		//_Metallic("Metalness", Range(0, 1)) = 0
		[HDR] _Emission("Emission", color) = (0,0,0)

		[Header()][Header(Dissolve Settings)]
		_DissolveTex("Dissolve Texture", 2D) = "black" {}
		_DissolveAmount("Dissolve Amount", Range(0, 0.999)) = 0.5

		[Header()][Header(Dissolve Edge Settigns)]
		[HDR]_EdgeColor("Color", Color) = (1, 1, 1, 1)
		_EdgeRange("Range", Range(0, .3)) = 0.1
		_EdgeFalloff("Falloff", Range(0.001, .3)) = 0.1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+1"}
		//Ztest checks zbuffer for if fragments are in the same place. 
		//checking the depth of the fragment that is currently rendered
		//if the fragment is equal to a current fragment overwrite it
		ZTest LEqual

//

        Pass
        {
		    CGPROGRAM

    		#pragma vertex vert 
            #pragma geometry geo 
            #pragma fragment frag
	    	#pragma target 3.0

            #include "UnityCG.cginc"
    
	    	sampler2D _MainTex;
		    sampler2D _NormalMap;
     		sampler2D _SpecularMap;

    		float2 uv_NormalMap;	

	    	half _Smoothness;
    		half _SnowAmount;
	    	half3 _Emission;
	
	    	sampler2D _DissolveTex;
	    	float _DissolveAmount;
            fixed4 _Color;

	    	float3 _EdgeColor;
		    float _EdgeRange;
		    float _EdgeFalloff;

            struct appdata
            {
                float2 uv_MainTex : TEXCOORD0;
	    		float4 pos : POSITION0;
	    		float3 vertexNormal : NORMAL0;
            };

	
	    	struct v2g
	    	{
	    		float2 uv_MainTex : TEXCOORD1;
	    		float4 pos : POSITION1;
	    		float3 vertexNormal : NORMAL1;
            };

            struct g2f
            { 
	    		float2 uv_MainTex : TEXCOORD2;
	    		float4 pos : POSITION2;
	    		float3 vertexNormal : NORMAL2;
            };


            //---------------
            //VERTEX SHADER 	  transfer the vertex normlal to the input structure
            //---------------
	    	v2g vert (inout appdata v) 
	    	{
                v2g o;
	    		//UNITY_INITIALIZE_OUTPUT(v2g,o);
	    		o.vertexNormal = mul(unity_ObjectToWorld, v.vertexNormal);
	    		o.pos = v.pos * 0.5;

                return o;
	    	}

            //---------------
            //GEOMETRY SHADER 	  t
            //---------------

            [maxvertexcount(12)]
            void geo (triangle v2g v[3], inout TriangleStream<g2f> tristream)
            {
               tristream.Append(v[0]);
               tristream.Append(v[1]);
               tristream.Append(v[2]);
            }

            //---------------
            //FRAGMENT SHADER 	  t
            //---------------
	    	fixed4 frag(g2f i) : SV_Target
	    	{
                //main texture settings
		    	fixed4 col = tex2D(_MainTex, i.pos.xz);
		    	col *= _Color;



	    		//for the dissolving texture
	    		float dissolve = tex2D(_DissolveTex, i.pos.xz).r;
	    		dissolve = dissolve * 0.999;
	      		float isVisible = dissolve - _DissolveAmount;
	    		clip(isVisible);
	    		//float x = noise(float4(1));

		    	//edge for the dissolve texture
		    	float isedge = smoothstep(_EdgeRange + _EdgeFalloff, _EdgeRange, isVisible);
		    	float3 edge = isedge * _EdgeColor;
	
			
		    	float4 worldNormal;
			
		    	//discard the normal map that isnt facing upwards
	    		if (i.vertexNormal.y < _SnowAmount + ((i.pos.x * i.pos.z) % 1.0) / 2.0)
		    	{
		    		discard;
		    	}

                return col;
            }
            ENDCG
		}
	}
	//thanks kieran
	//thanks kieran
	//thanks kieran
	//thanks kieran
	//thanks kieran
}
